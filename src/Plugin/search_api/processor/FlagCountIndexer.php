<?php

namespace Drupal\flag_search_api\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\Item\ItemInterface;

/**
 * Search API Processor for indexing flag counts.
 *
 * @SearchApiProcessor(
 *   id = "flag_count_indexer",
 *   label = @Translation("Flag count indexing"),
 *   description = @Translation("Switching on will enable indexing flag counts
 *   on content."), stages = {
 *     "add_properties" = 1,
 *     "pre_index_save" = -10
 *   }
 * )
 */
class FlagCountIndexer extends FlagIndexer {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['flag_index']['#description'] = $this->t('Index overall count of users who flagged this content.');
    return $form;
  }

  /**
   * Helper function for defining our custom fields.
   */
  protected function getFieldsDefinition() {
    $config = $this->configuration['flag_index'];
    $fields = [];
    foreach ($config as $flag) {
      $label = $this->flagService->getFlagById($flag)->get('label');
      $fields['flag_' . $flag . '_count'] = array(
        'label' => $this->t('@label count', ['@label' => $label]),
        'description' => $label,
        'type' => 'integer',
        'processor_id' => $this->getPluginId(),
      );
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $config = $this->configuration['flag_index'];
    $flags = $this->flagService->getAllFlags();
    try {
      $entity = $item->getOriginalObject()->getValue();
      foreach ($config as $flag_id) {
        $fields = $this
          ->getFieldsHelper()
          ->filterForPropertyPath($item->getFields(), NULL, 'flag_' . $flag_id . '_count');

        foreach ($fields as $flag_field) {
          $users = $this->flagService->getFlaggingUsers($entity, $flags[$flag_id]);
          $flag_field->addValue(count($users));
        }
      }
    }
    catch (SearchApiException $exception) {
      $this->logger->error($exception->getMessage());
    }

  }

}
